﻿using System;

namespace LV7
{
    class Program
    {
        static void Main(string[] args)
        {
            var rand = new Random();
            double[] array = new double[20];
            //double[] unsortedArray = new double[20];
            
            for(int i=0;i<20;i++)
            {
                array[i] = rand.NextDouble() + rand.Next(100);
            }
            //array.CopyTo(unsortedArray, 0);
            SequentialSort sequentialSort = new SequentialSort();
            CombSort combSort = new CombSort();
            BubbleSort bubbleSort = new BubbleSort();

            NumberSequence numberSequence = new NumberSequence(array);
            numberSequence.SetSortStrategy(sequentialSort);
            numberSequence.Sort();
            Console.WriteLine("Seq sort: \n" + numberSequence.ToString());

            numberSequence = new NumberSequence(array);
            numberSequence.SetSortStrategy(combSort);
            numberSequence.Sort();
            Console.WriteLine("Comb sort: \n" + numberSequence.ToString());

            numberSequence = new NumberSequence(array);
            numberSequence.SetSortStrategy(bubbleSort);
            numberSequence.Sort();
            Console.WriteLine("Bubble sort: \n" + numberSequence.ToString());
        }
    }
}
