﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD2
{
    class BubbleSort:SortStrategy
    {
        public override void Sort(double[] array)
        {
            int arraySize = array.Length;
            for (int j = 0; j <= arraySize - 2; j++)
            {
                for (int i = 0; i <= arraySize - 2; i++)
                {
                    if (array[i] > array[i + 1])
                    {
                        Swap(ref array[i],ref array[i + 1]);
                    }
                }
            }
        }
    }
}
