﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZAD2
{
    class LinearSearch:SearchStrategy
    {
        public int Search(double element, double[] array)
        {
            int arraySize = array.Length;
            for (int i = 0; i < arraySize; i++)
            {
                if (array[i] == element)
                {
                    Console.WriteLine("Element pronađen na " + (i+1) + ". mjestu.");
                    return i;
                }
            }
            Console.WriteLine("Element nije pronađen :(");
            return -1;
        }
    }
}
