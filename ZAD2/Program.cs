﻿using System;

namespace ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            var rand = new Random();
            double[] array = new double[20];
            for (int i = 0; i < 20; i++)
            {
                array[i] = rand.NextDouble() + rand.Next(100);
            }
            array[5] = 20;
            LinearSearch linear = new LinearSearch();
            NumberSequence numberSequence = new NumberSequence(array);
            numberSequence.SetSearchStrategy(linear);
            numberSequence.Search(20);
        }
    }
}
