﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ZAD2
{
    interface SearchStrategy
    {
        public int Search(double element, double[] array);
    }
}
