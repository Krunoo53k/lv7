﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD4
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider provider = new SystemDataProvider();
            ConsoleLogger consoleLogger = new ConsoleLogger();
            FileLogger fileLogger = new FileLogger("log.txt");         
            while(true)
            {
                consoleLogger.Log(provider);
                fileLogger.Log(provider);
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
