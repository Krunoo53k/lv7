﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD5
{
    interface IItem
    {
        double Accept(IVisitor visitor);
    }
}
