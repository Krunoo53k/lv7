﻿using System;

namespace ZAD5
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book("Ratimir", 100);
            DVD dvd = new DVD("Shrek 5", 0, 35);
            VHS vhs = new VHS("Preko Ograde", 25);
            BuyVisitor buy = new BuyVisitor();
            Console.WriteLine(vhs.Accept(buy));
            Console.WriteLine(dvd.Accept(buy));
            Console.WriteLine(book.Accept(buy));
        }
    }
}
