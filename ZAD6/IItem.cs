﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD6
{
    interface IItem
    {
        double Accept(IVisitor visitor);
    }
}
