﻿using System;

namespace ZAD6
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book("Ratimir", 100);
            DVD dvd = new DVD("Shrek 5", 0, 35);
            VHS vhs = new VHS("Preko Ograde", 25);
            IVisitor rent = new RentVisitor();
            Console.WriteLine(vhs.Accept(rent));
            Console.WriteLine(dvd.Accept(rent));
            Console.WriteLine(book.Accept(rent));
        }
    }
}
