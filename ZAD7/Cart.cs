﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;

namespace ZAD7
{
    class Cart:IItem
    {
        private List<IItem> items;
        public Cart()
        {
            this.items = new List<IItem>();
        }
        public void Add(IItem item)
        {
            items.Add(item);
        }
        public void Remove(IItem item)
        {
            items.Remove(item);
        }
        public double Accept(IVisitor visitor)
        {
            double totalCost = 0;
            foreach(IItem item in items)
            {
                totalCost += item.Accept(visitor);
            }
            return totalCost;
        }
    }
}
