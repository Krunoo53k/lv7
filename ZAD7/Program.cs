﻿using System;

namespace ZAD7
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book("Ratimir", 100);
            DVD dvd = new DVD("Shrek 5", 0, 35);
            VHS vhs = new VHS("Preko Ograde", 25);
            IVisitor rent = new RentVisitor();
            IVisitor buy = new BuyVisitor();
            Cart cart = new Cart();
            cart.Add(book);
            cart.Add(dvd);
            cart.Add(vhs);
            Console.WriteLine(cart.Accept(rent));
            Console.WriteLine(cart.Accept(buy));
        }
    }
}
