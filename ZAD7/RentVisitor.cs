﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD7
{
    class RentVisitor:IVisitor
    {
        private double rentRatio = 0.10;
        public double Visit(Book book)
        {
            return book.Price * rentRatio;
        }
        public double Visit(DVD dvd)
        {
            if (dvd.Type == 0)
            {
                return dvd.Price;
            }
            else
            {
                return dvd.Price * rentRatio;
            }
        }
        public double Visit(VHS vhs)
        {
            return vhs.Price * rentRatio;
        }
    }
}
